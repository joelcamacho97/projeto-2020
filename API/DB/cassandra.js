const cassandra = require('cassandra-driver');
const Uuid = require('cassandra-driver').types.Uuid;
const id = Uuid.random();


const client = new cassandra.Client({ 
    contactPoints: ['127.0.0.1:9042'],
    localDataCenter: 'datacenter1'
  });
  
  client.connect(function (err) {
    console.log(err);
  });

var tozero = function(row){
    if(row == "") return 0
    else return row
}


module.exports = {

    KEY: async function (KEYSPACE) {

        await client.connect(async function(err) {
            if (!err) {
                var query = "CREATE KEYSPACE IF NOT EXISTS " + KEYSPACE + " WITH replication = {\'class\': \'NetworkTopologyStrategy\', \'datacenter1\' : \'1\' }";
                await client.execute(query, [],function(err) {
                    if (err) {
                        console.log("error in keyspace creation");
                        console.log(err);
                    }
                });
            }
        
        });
    
    },


    TABLE: async function (KEYSPACE,TABLE) {

        var query = "CREATE TABLE IF NOT EXISTS "+KEYSPACE+"."+TABLE+" (id uuid PRIMARY KEY,iid int,tmstp ascii,deploy float,Imin float,Imax float,Iavg float,Vmin float,Vmax float,Vavg float,Pmin float,Pmax float,Pavg float,Qmin float,Qmax float,Qavg float,PFmin float,PFmax float,PFavg float,miss_flag int)";      
        await client.execute(query);
    
    },

    inserir: async function (KEYSPACE,TABLE,csv) {

            var query = 'INSERT INTO  '+KEYSPACE+'.'+TABLE+' (id,iid,tmstp,deploy,Imin,Imax,Iavg,Vmin,Vmax,Vavg,Pmin,Pmax,Pavg,Qmin,Qmax,Qavg,PFmin,PFmax,PFavg,miss_flag) VALUES (now(),'+csv.iid+','+"'"+tozero(csv.tmstp)+"'"+','+tozero(csv.deploy)+','+tozero(csv.Imin)+','+tozero(csv.Imax)+','+tozero(csv.Iavg)+','+tozero(csv.Vmin)+','+tozero(csv.Vmax)+','+tozero(csv.Vavg)+','+tozero(csv.Pmin)+','+tozero(csv.Pmax)+','+tozero(csv.Pavg)+','+tozero(csv.Qmin)+','+tozero(csv.Qmax)+','+tozero(csv.Qavg)+','+tozero(csv.PFmin)+','+tozero(csv.PFmax)+','+tozero(csv.PFavg)+','+tozero(csv.miss_flag)+')'
            
            //console.log(query);
            await client.execute(query, [],function(err) {
                if (err) {
                    console.log(err);
                }
            });
            //await client.execute(query);
    }
};


