import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import {Component, ViewChild, AfterViewInit, OnInit, Input} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { formatDate } from '@angular/common';
import LocalePT from '@angular/common/locales/pt-PT';
import { registerLocaleData } from '@angular/common';
registerLocaleData(LocalePT, "pt-PT");



 
@Component({
  selector: 'app-teste',
  templateUrl: './teste.component.html',
  styleUrls: ['./teste.component.css']
})

export class TesteComponent {

  former: FormGroup;
    constructor(private fb: FormBuilder) {
      let now = Date.now();
        var createdAt = formatDate(now, 'yyyy-MM-dd HH:mm:ss', 'pt-PT');
		
      this.former = this.fb.group({
      idd: ['48', Validators.required],
      deploy: ['4', Validators.required],
      tmstp: [createdAt, Validators.required],
      missflag: ['0',Validators.required],
      Imin: [0, Validators.required],
      Imax: [0, Validators.required],
      Iavg: [0, Validators.required],
      Vmin: [0, Validators.required],
      Vmax: [0, Validators.required],
      Vavg: [0, Validators.required],
      Pmin: [0, Validators.required],
      Pmax: [0, Validators.required],
      Pavg: [0, Validators.required],
      Qmin: [0, Validators.required],
      Qmax: [0, Validators.required],
      Qavg: [0, Validators.required],
      PFmin: [0, Validators.required],
      PFmax: [0, Validators.required],
      PFavg: [0, Validators.required]
    });

  }
 	
    ngOnInit(): void {
        
  }

  public changeListener(files: FileList){
  console.log(files);
  if(files && files.length > 0) {
     let file : File = files.item(0); 
       console.log(file.name);
       console.log(file.size);
       console.log(file.type);
       let reader: FileReader = new FileReader();
       reader.readAsText(file);
       reader.onload = (e) => {
          let csv: string = reader.result as string;
          console.log(csv);
       }
    }
}


  onSubmit () {

  	console.log(this.former.value);
  }
 
}
