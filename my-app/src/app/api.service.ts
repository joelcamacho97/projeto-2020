import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})



export class ApiService {

  downloadFile(data, filename='data') {
    console.log()
    let csvData = this.ConvertToCSV(data,  ['iid','Iavg','Imax','Imin','PFavg','PFmax','PFmin','Pavg','Pmax','Pmin','Qavg','Qmax','Qmin','Vavg','Vmax','Vmin','deploy','miss_flag','tmstp' ]);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
        dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
}

ConvertToCSV(objArray, headerList) {
     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
     let str = '';
     let row = '';

     for (let index in headerList) {
         row += headerList[index] + ',';
     }
     row = row.slice(0, -1);
     str += row + '\r\n';
     for (let i = 0; i < array.length; i++) {
         let line = '';
         for (let index in headerList) {
            let head = headerList[index];
            //console.log(array[i]["_source"][head] + ',');
            line += array[i]["_source"][head] + ',';
         }
         line = line.slice(0, -1);
         str += line + '\r\n';
     }
     return str;
 }
 
  constructor(private httpClient: HttpClient) {
    
   }
}
