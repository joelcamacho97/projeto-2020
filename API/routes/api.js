var express = require('express');
var router = express.Router();
var db = require('../DB/multi-db');
const neatCsv = require('neat-csv');
const fs = require('fs');


fs.readFile('./data_2.csv', async (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const csv = await neatCsv(data);
  console.log("Ficheiro lido!");
  db.inserir(csv);
});

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
