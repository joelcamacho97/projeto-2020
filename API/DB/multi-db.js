var elastic = require('./elastic');
var cassandra = require('./cassandra');

module.exports = {
    
    inserir: async function (csv) {

        await cassandra.KEY('teste');
        await cassandra.TABLE('teste','teste');

        for (let index = 0; index < csv.length; index++) {
            console.log((index*100)/csv.length);

            await cassandra.inserir('teste','teste',csv[index]); // (keyspace,tabela,dados)
            await elastic.inserir('teste',csv[index]); // (index,dados) funcionando!
        }
    }
}
