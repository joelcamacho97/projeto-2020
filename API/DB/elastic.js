var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace',
  apiVersion: '7.6'
});

client.ping({
    requestTimeout: 1000
  }, function (error) {
    if (error) {
      console.trace('elasticsearch cluster is down!');
    } else {
      console.log('All is well');
    }
});

var tozero = function(row){
  if(row == "") return 0
  else return row
}
  
module.exports = {
    
    inserir: async function (nome, csv) {
              await client.index({
                index: nome,
                body: {
                  iid: tozero(csv.iid),
                  tmstp: tozero(csv.tmstp),
                  deploy: tozero(csv.deploy),
                  Imin: tozero(parseFloat(csv.Imin)),
                  Imax: tozero(parseFloat(csv.Imax)),
                  Iavg: tozero(parseFloat(csv.Iavg)),
                  Vmin: tozero(parseFloat(csv.Vmin)),
                  Vmax: tozero(parseFloat(csv.Vmax)),
                  Vavg: tozero(parseFloat(csv.Vavg)),
                  Pmin: tozero(parseFloat(csv.Pmin)),
                  Pmax: tozero(parseFloat(csv.Pmax)),
                  Pavg: tozero(parseFloat(csv.Pavg)),
                  Qmin: tozero(parseFloat(csv.Qmin)),
                  Qmax: tozero(parseFloat(csv.Qmax)),
                  Qavg: tozero(parseFloat(csv.Qavg)),
                  PFmin: tozero(parseFloat(csv.PFmin)),
                  PFmax: tozero(parseFloat(csv.PFmax)),
                  PFavg: tozero(parseFloat(csv.PFavg)),
                  miss_flag: tozero(csv.miss_flag)
                }
            })
      }
};
