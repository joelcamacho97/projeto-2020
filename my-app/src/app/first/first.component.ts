import {Component, ViewChild, AfterViewInit, OnInit, Input} from '@angular/core';
import { ApiService } from '../api.service';

import {HttpClient} from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {merge, Observable, of as observableOf} from 'rxjs';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';

import {MatDatepickerInputEvent, MatDatepicker, MatDatepickerInput, MatDatepickerIntl} from '@angular/material/datepicker';
import { MatFormField } from '@angular/material/form-field';
import { stringToKeyValue } from '@angular/flex-layout/extended/typings/style/style-transforms';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})


export class FirstComponent implements AfterViewInit    {

 chart_Iavg = [];
 chart_Imax = [];
 chart_Imin = [];
 chart_PFavg = [];
 chart_PFmax = [];
 chart_PFmin = [];
 chart_Pmax = [];
 chart_Pmin = [];
 chart_Qavg = [];
 chart_Qmax = [];
 chart_Qmin = [];
 chart_Vmax = [];
 chart_Vmin = [];
 chart_tmstp = [];

  calendario = '';
      
 
  displayedColumns: string[] = [
                                'iid',
                                'Iavg',
                                'Imax',
                                'Imin',
                                'PFavg',
                                'PFmax',
                                'PFmin',
                                'Pavg',
                                'Pmax',
                                'Pmin',
                                'Qavg',
                                'Qmax',
                                'Qmin',
                                'Vavg',
                                'Vmax',
                                'Vmin',
                                'deploy',
                                'miss_flag',
                                'tmstp' ];
                                
  
  exampleDatabase: ExampleHttpDatabase | null;
  data: GithubIssue[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatDatepicker) calendario1: MatDatepicker<Date>;

  constructor(private _httpClient: HttpClient, private ApiService:ApiService) {}

  ngAfterViewInit() {
    this.exampleDatabase = new ExampleHttpDatabase(this._httpClient);
    
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
   
    //console.log("ola"+merge(this.sort.sortChange, this.paginator.page))
    merge(this.sort.sortChange, this.paginator.page, this.calendario1.closedStream)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.exampleDatabase!.getRepoIssues(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.calendario);
        }),
        map(data => {
          
          console.log("calendario : " + this.calendario)

          
          
          /*for (let i in data["hits"]["hits"]){
            
            this.chart_Iavg.push('"'+data["hits"]["hits"][i]["_source"]["Iavg"]+'"');
            this.chart_Imax.push(data["hits"]["hits"][i]["_source"]["Imax"]);
            this.chart_Imin.push(data["hits"]["hits"][i]["_source"]["Imin"]);
            this.chart_PFavg.push(data["hits"]["hits"][i]["_source"]["PFavg"]);
            this.chart_PFmax.push(data["hits"]["hits"][i]["_source"]["PFmax"]);
            this.chart_PFmin.push(data["hits"]["hits"][i]["_source"]["PFmin"]);
            this.chart_Pmax.push(data["hits"]["hits"][i]["_source"]["Pmax"]);
            this.chart_Pmin.push(data["hits"]["hits"][i]["_source"]["Pmin"]);
            this.chart_Qavg.push(data["hits"]["hits"][i]["_source"]["Qavg"]);
            this.chart_Qmax.push(data["hits"]["hits"][i]["_source"]["Qmax"]);
            this.chart_Qmin.push(data["hits"]["hits"][i]["_source"]["Qmin"]);
            this.chart_Vmax.push(data["hits"]["hits"][i]["_source"]["Vmax"]);
            this.chart_Vmin.push(data["hits"]["hits"][i]["_source"]["Vmin"]);
            this.chart_tmstp.push(data["hits"]["hits"][i]["_source"]["tmstp"])

          }*/

          //console.log("tmstp : " + this.chart_tmstp)
          //console.log("ola" + data.hits["hits"][]._source.iid)
    


          this.isLoadingResults = false;
          this.isRateLimitReached = false;
          //this.resultsLength = data.hits["total"]
          this.resultsLength = data.hits["hits"].length;


          return data.hits["hits"];
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => this.data = data);
      

      
  }

  download(){
   console.log(this.data)
    this.ApiService.downloadFile(this.data, 'dados');
  }

  limpar(){
  
    this.data = [];
    this.calendario = '';
    this.exampleDatabase!.getRepoIssues(
            this.sort.active, this.sort.direction, this.paginator.pageIndex, this.calendario).toPromise().then(data => {
              console.log(data);
              this.data = data.hits["hits"];
            });
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
  //this.events.push(`${type}: ${event.value}`); 
  this.calendario = 'q=tmstp:"'+this.formatDate(event.value).toString()+'"';
  //this.calendario = this.calendario;
  //+'"&size=10000'
  //console.log(type)

}


}

  export interface GithubApi {
    hits: GithubIssue[];
    total: number;
  }
  
  export interface GithubIssue {
    _source: {
      iid: number,
      Iavg: number,
      Imax: number,
      Imin: number,
      PFavg: number,
      PFmax: number,
      PFmin: number,
      Pavg: number,
      Pmax: number,
      Pmin: number,
      Qavg: number,
      Qmax: number,
      Qmin: number,
      Vavg: number,
      Vmax: number,
      Vmin: number,
      deploy: number,
      miss_flag: number,
      tmstp: string
    }
  }
  
  /** An example database that the data source uses to retrieve data for the table. */
  export class ExampleHttpDatabase {
    constructor(private _httpClient: HttpClient) {}

   
    
    getRepoIssues(sort: string, order: string, page: number, qdate: string): Observable<GithubApi> {


      const href = 'https://api.github.com/search/issues';
      const requestUrl =
          `${href}?q=repo:angular/components&sort=&order=${order}&page=${page + 1}`;
      var apiURL = 'http://localhost:9200/teste/_search?';
      const headerDict = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
      };  
      
      const requestOptions = {                                                                                                                                                                                 
        headers: new Headers(headerDict), 
      };

      return this._httpClient.get<GithubApi>(apiURL+qdate);
    }

  }
  

