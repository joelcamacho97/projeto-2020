import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstComponent } from './first/first.component';
import { TesteComponent } from './teste/teste.component';
import { KibanaComponent } from './kibana/kibana.component';

const routes: Routes = [
  { path: 'first', component: FirstComponent },
  { path: 'teste', component: TesteComponent },
  { path: 'kibana', component: KibanaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
